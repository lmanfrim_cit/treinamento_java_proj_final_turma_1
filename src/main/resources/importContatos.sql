insert into sistema(id, descricao) VALUES (1, 'Contas Médicas');
insert into sistema(id, descricao) VALUES (2, 'Prestador');
insert into sistema(id, descricao) VALUES (3, 'DERESS');
insert into sistema(id, descricao) VALUES (4, 'Centros Clínicos');
insert into sistema(id, descricao) VALUES (5, 'Interodonto');

insert into telefone (id, ddd, telefone, tipo) values (1, 11, 31552245, 'Trabalho');
insert into telefone (id, ddd, telefone, tipo) values (2, 11, 31552413, 'Trabalho');
insert into telefone (id, ddd, telefone, tipo) values (3, 11, 31552681, 'Trabalho');
insert into telefone (id, ddd, telefone, tipo) values (4, 11, 999467832, 'Celular');
insert into telefone (id, ddd, telefone, tipo) values (5, 11, 998540912, 'Celular');

insert into contato (id, email, nome, sistema_id) values (1, 'claudia.omezo@intermedica.com.br', 'Claudia Omezo', 1);
insert into contato (id, email, nome, sistema_id) values (2, 'izaias.romano@intermedica.com.br', 'Izaias Romano', 5);
insert into contato (id, email, nome, sistema_id) values (3, 'isa@intermedica.com.br', 'Isa', 4);
insert into contato (id, email, nome, sistema_id) values (4, 'fatima@intermedica.com.br', 'Fátima', 3);
insert into contato (id, email, nome, sistema_id) values (5, 'martinho@intermedica.com.br', 'Martinho', 2);

insert into contato_telefone (contato_id, telefone_id) values (1, 1);
insert into contato_telefone (contato_id, telefone_id) values (2, 3);
insert into contato_telefone (contato_id, telefone_id) values (3, 2);
insert into contato_telefone (contato_id, telefone_id) values (4, 5);
insert into contato_telefone (contato_id, telefone_id) values (5, 4);
insert into contato_telefone (contato_id, telefone_id) values (5, 3);
