package br.com.ciandt.dojoTurma1.dao;
import br.com.ciandt.dojoTurma1.entity.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContatoRepository extends JpaRepository<Contato, Long>{
}
