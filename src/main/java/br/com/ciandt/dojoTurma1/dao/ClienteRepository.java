package br.com.ciandt.dojoTurma1.dao;

import br.com.ciandt.dojoTurma1.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ClienteRepository extends JpaRepository<Cliente, Long>{
}
