package br.com.ciandt.dojoTurma1.service;

import br.com.ciandt.dojoTurma1.entity.Contato;

import java.util.List;
import java.util.Optional;

public interface IContatoService {

    List<Contato> listarContatos();

    Optional<Contato> buscarContato(Long id);

    Contato salvar(Contato contato);

    void deletar(Long id);

}
