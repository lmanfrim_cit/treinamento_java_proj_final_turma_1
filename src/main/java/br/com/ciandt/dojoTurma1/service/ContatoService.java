package br.com.ciandt.dojoTurma1.service;

import br.com.ciandt.dojoTurma1.dao.ContatoRepository;
import br.com.ciandt.dojoTurma1.entity.Contato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("abobrinha")
public class ContatoService implements IContatoService {

    @Autowired
    ContatoRepository repository;

    @Override
    public List<Contato> listarContatos() {
        return repository.findAll();
    }

    @Override
    public Optional<Contato> buscarContato(Long id) {
        return repository.findById(id);
    }

    @Override
    public Contato salvar(Contato contato) {
        return repository.save(contato);
    }

    @Override
    public void deletar(Long id) {
        repository.deleteById(id);
    }

}
