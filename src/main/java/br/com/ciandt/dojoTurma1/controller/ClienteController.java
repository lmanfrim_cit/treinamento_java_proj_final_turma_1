package br.com.ciandt.dojoTurma1.controller;

import br.com.ciandt.dojoTurma1.dao.ClienteRepository;
import br.com.ciandt.dojoTurma1.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController  {

    @Autowired
    ClienteRepository repository;

    @GetMapping
    public List<Cliente> clientes() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Cliente> cliente(@PathVariable("id") Long id) {
        return repository.findById(id);
    }



}
