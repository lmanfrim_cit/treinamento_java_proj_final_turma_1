package br.com.ciandt.dojoTurma1.controller;

import br.com.ciandt.dojoTurma1.entity.Contato;
import br.com.ciandt.dojoTurma1.service.IContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class ContatoController {

    @Autowired
    @Qualifier("abobrinha")
    IContatoService service;


    @GetMapping
    public List<Contato> listarContatos(){
        return service.listarContatos();
    }

    @GetMapping("/{id}")
    public Optional<Contato> buscarContato(@PathVariable("id") Long id){
        return service.buscarContato(id);
    }

    @PostMapping()
    public Contato inserirContato(@RequestBody Contato contato){
        return service.salvar(contato);
    }

    @DeleteMapping("/{id}")
    public void deletarContato(@PathVariable("id") Long id){
        service.deletar(id);
    }

    @PutMapping()
    public Contato alterarContato(@RequestBody Contato contato){
        return service.salvar(contato);
    }

}
